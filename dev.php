<?php
/**
 * THIS FILE FOR AUTHORIZED DEVELOPERS ONLY!
 *
 * THESE ARE USEFUL TOOLS FOR MAGENTO.
 * DO NOT MODIFY CODE UNTIL YOU KNOW WHAT ARE YOU DOING.
 *
 * THIS FILE SHOULD BE CHANGED PASSWORD OR REMOVE AFTER DEVELOPMENT. WE DO NOT RESPONSE TO ANY RESULT.
 *
 */
require_once 'app/autoload.php';
require 'app/bootstrap.php';
?>
<?php

class Dev
{
    const USERNAME = 'admin';
    const PASSWORD = 'admin!@#';
    protected $_objectManager;
    protected $_storeManager;
    protected $_scopeConfig;

    /**
     * Magento 2 Developer Tools
     */
    public function __construct()
    {
        $this->authorize();
        $this->initBootstrap();
        $this->setAreaCode('frontend');
        $context             = $this->getContext();
        $this->_storeManager = $context->getStoreManager();
        $this->_scopeConfig  = $context->getScopeConfig();
        $this->initAction();
    }

    /**
     * Init Bootstrap
     */
    private function initBootstrap()
    {
        $bootstrap = \Magento\Framework\App\Bootstrap::create(BP, $_SERVER);
        $bootstrap->createApplication('Magento\Framework\App\Http');
        $this->_objectManager = $bootstrap->getObjectManager();
    }

    /**
     * @return mixed
     */
    public function initAction()
    {
        $action = $this->getAction();
        if ($action) {
            return $this->$action();
        }
    }

    /**
     * Authentication user
     */
    private function authorize()
    {
        if (!isset($_COOKIE['isLoggedIn']) AND !$this->_isLocal() AND ($_SERVER['PHP_AUTH_PW'] != self::PASSWORD || $_SERVER['PHP_AUTH_USER'] != self::USERNAME) || !$_SERVER['PHP_AUTH_USER']) {
            header('WWW-Authenticate: Basic realm="Test auth"');
            header('HTTP/1.0 401 Unauthorized');
            echo 'Auth failed';
            exit;
        }
        setcookie('isLoggedIn', 1, time() + 86400 * 36500, '/');
    }

    /**
     * Check is local environment
     *
     * @return bool
     */
    protected function _isLocal()
    {
        return (in_array($_SERVER['SERVER_NAME'],
            array(
                'localhost', 'localhost.com', 'www.localhost.com', 'm.com', 'test.loc', 'mage.loc',
            )
        ));
    }

    /**
     * get Dev Url
     *
     * @param array $params
     * @return string
     */
    public function getUrl($params = array())
    {
        $routePath = basename(__FILE__);
        $cnt       = 0;
        foreach ($params as $key => $value) {
            if (is_null($value) || false === $value || '' === $value || !is_scalar($value)) {
                continue;
            }
            $cnt++;
            if ($cnt == 1) {
                $routePath .= '?' . $key . '=' . $value;
            } else {
                $routePath .= '&&' . $key . '=' . $value;
            }
        }

        return $routePath;
    }

    /**
     * get action from request Url
     *
     * @return string
     */
    public function getAction()
    {
        return isset($_REQUEST['action']) ? $_REQUEST['action'] : '';
    }

    /**
     * Add message to session
     *
     * @param null $msg
     */
    public function addMessage($msg = null, $type = 0)
    {
        if (is_string($msg)) {
            $_SESSION['message'] = $msg . '<br>';
            if ($type == 1) {
                $_SESSION['message_type'] = 'success-message';
            } else if ($type == 2) {
                $_SESSION['message_type'] = 'notice-message';
            } else {
                $_SESSION['message_type'] = 'default-message';
            }
        }
    }


    /**
     * @return mixed
     */
    public function getResourceConfig()
    {
        return $this->getObjectManager()->get('Magento\Config\Model\ResourceModel\Config');
    }

    /**
     * Get current store ID
     *
     * @return mixed
     */

    public function getStoreId()
    {
        return $this->_storeManager->getStore()->getId();
    }

    /**
     * get store config value
     *
     * @param      $config
     * @param null $storeId
     * @return mixed
     */
    public function getStoreConfig($path, $storeId = null, $scope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE)
    {
        return $this->_scopeConfig->getValue(
            $path,
            $scope,
            $storeId
        );
    }

    /**
     * @param      $path
     * @param      $value
     * @param null $storeId
     */
    public function setStoreConfig()
    {
        $storeId = $this->getStoreId();
        $path    = isset($_GET['path']) ? $_GET['path'] : null;
        $value   = isset($_GET['value']) ? $_GET['value'] : null;
        if (isset($path) && isset($value)) {
            $this->saveConfig($path, $value, $storeId);
            $this->addMessage($path . ' is saved!', 1);
        }
    }

    /**
     * @param      $path
     * @param null $storeId
     */
    public function saveConfig($path, $value, $storeId = null, $scope = \Magento\Framework\App\Config\ScopeConfigInterface::SCOPE_TYPE_DEFAULT)
    {
        $this->getResourceConfig()->saveConfig($path,
            $value,
            $scope,
            $storeId);
        if (strpos($path, 'modules_disable_output') != false) {

        }
        $this->getStoreManager()->getStore()->resetConfig();
    }

    /**
     * Set area
     *
     * @param $code
     */
    public function setAreaCode($code)
    {
        $this->getObjectManager()->get('Magento\Framework\App\State')->setAreaCode($code);
    }

    /**
     * get object manager
     *
     * @return mixed
     */
    public function getObjectManager()
    {
        return $this->_objectManager;
    }

    /**
     * @return mixed
     */
    public function getStoreManager()
    {
        return $this->_storeManager;
    }

    /**
     * Get base url
     *
     * @return mixed
     */
    public function getBaseUrl()
    {
        return $this->getStoreManager()->getStore()->getBaseUrl();
    }

    /**
     * @return mixed
     */
    public function getContext()
    {
        return $this->getObjectManager()->get('\Magento\Framework\View\Element\Template\Context');
    }

    /**
     * get cache manager
     *
     * @return mixed
     */
    public function getCacheManager()
    {
        return $this->getObjectManager()->get('\Magento\Framework\App\Cache\Manager');
    }

    /**
     * Magento and PHP information
     */
    public function getBasicConfigs()
    {
        echo '<span class="button">Note</span> <a href="' . $this->getUrl(array('action' => 'backup')) . '">Run Backup before making changes</a>';
        echo '<hr />';
        $configs = array(
            'web/unsecure/base_url',
            'web/secure/base_url',
            'web/cookie/cookie_lifetime',
            'design/head/default_title',
            'design/theme/theme_id',
            'design/search_engine_robots/default_robots',
            'design/header/logo_src',
            'general/locale/code',
            'design/header/welcome',
            'design/footer/copyright',
            'customer/account_share/scope',
            'dev/restrict/allow_ips',
            'dev/debug/template_hints_storefront',
            'dev/debug/template_hints_admin',
            'dev/debug/template_hints_blocks',
            'dev/translate_inline/active',
            'dev/translate_inline/active_admin',
            'dev/js/merge_files',
            'dev/js/minify_files',
            'dev/css/merge_css_files',
            'dev/css/minify_files',
            'admin/security/use_form_key'
        );
        echo '<div class="basic-config-wrapper" style="height: 450px;">';
        echo '<div style="width: 60%;float:left">';
        echo '<span><strong>General Config:</strong></span><br />';
        $storeId = $this->getStoreId();
        foreach ($configs as $config) {
            $value = $this->getStoreConfig($config, $storeId);
            if ($config == 'design/theme/theme_id') {
                $value = $this->getThemeLabel($value);
            }
            echo '<span class="info"><span class="config-name"><a href="' . $this->getUrl(array('action' => 'setStoreConfig',
                                                                                                'path'   => urlencode($config),
                                                                                                'value'  => $this->getStoreConfig($config, $storeId)
                )) . '">' . $config . '</a></span>
		: ' . $value . '</span><br>';
        }
        echo '</div>';//End div left
        echo '<div style="width:40%;float:left; ">';
        echo '<span><strong>Installed Extensions:</strong></span><br />';
        $moduleList = $this->getObjectManager()->get('\Magento\Framework\Module\ModuleListInterface');
        $modules    = $moduleList->getNames();
        foreach ($modules as $moduleName) {
            if ($moduleName === 'Mage_Adminhtml' || strpos($moduleName, 'Magento_') !== false) {
                continue;
            }
            $path  = 'advanced/modules_disable_output/' . $moduleName;
            $value = $this->getStoreConfig($path) == 0 ? 'Enabled' : 'Disabled';
            $color = $this->getStoreConfig($path) == 0 ? 'green' : 'red';
            echo '<span><a href="' . $this->getUrl(array('action' => 'setStoreConfig',
                                                         'path'   => urlencode($path),
                                                         'value'  => (int)$this->getStoreConfig($path)
                )) . '">' . $moduleName . '</a><strong style="color:' . $color . '"> (' . $value . ')</strong></span><br />';
        }
        echo '</div>';//End div right
        echo '</div>';
        echo '<div style="clear: both"></div>';

    }

    public function getThemeLabel($id)
    {
        $themes = $this->getObjectManager()->get('\Magento\Framework\View\Design\Theme\Label');
        $labels = $themes->getLabelsCollection();
        foreach ($labels as $label) {
            if ($label['value'] == $id)
                return $label['label'];
        }
    }

    /**
     * Enable frontend template path hint
     */

    public function frontendPH()
    {

        $storeId = $this->getStoreId();
        //Allow current IP
        $allowed_ips = $this->getStoreConfig('dev/restrict/allow_ips', $storeId);
        $ips         = array_unique(array_merge(explode(',', $allowed_ips), array($_SERVER['REMOTE_ADDR'])));
        $allowed_ips = empty($allowed_ips) ? $_SERVER['REMOTE_ADDR'] : implode(",", $ips);
        $this->saveConfig('dev/restrict/allow_ips', $allowed_ips, 'stores', $storeId); //Save config
        //Toggle
        $config = $this->getStoreConfig('dev/debug/template_hints_storefront', $storeId);
        if ($config == 0) {
            $this->addMessage('<strong>Enabled</strong> template path hints', 1);
            $this->saveConfig('dev/debug/template_hints_storefront', 1, $storeId);
            $this->saveConfig('dev/debug/template_hints_blocks', 1, $storeId);

        } else {
            $this->saveConfig('dev/debug/template_hints_storefront', 0, $storeId);
            $this->saveConfig('dev/debug/template_hints_blocks', 0, $storeId);
            $this->addMessage('<strong>Disabled</strong> template path hints', 2);
        }
        // Flush Cache
        $this->flushConfigCache();
    }

    /**
     * Enable backend template path hint
     */
    public function backendPH()
    {
        $storeId = 1;//Admin store
        //Allow current IP
        $allowed_ips = $this->getStoreConfig('dev/restrict/allow_ips', $storeId);
        $ips         = array_unique(array_merge(explode(',', $allowed_ips), array($_SERVER['REMOTE_ADDR'])));
        $allowed_ips = empty($allowed_ips) ? $_SERVER['REMOTE_ADDR'] : implode(",", $ips);
        $this->saveConfig('dev/restrict/allow_ips', $allowed_ips, $storeId);
        $config = $this->getStoreConfig('dev/debug/template_hints_admin', $storeId);
        if ($config == 0) {
            $this->saveConfig('dev/debug/template_hints_admin', 1, $storeId);
            $this->saveConfig('dev/debug/template_hints_blocks', 1, $storeId);
            $this->addMessage('<strong>Enabled</strong> Admin template path hints', 1);
        } else {
            $this->saveConfig('dev/debug/template_hints_admin', 0, $storeId);
            $this->saveConfig('dev/debug/template_hints_blocks', 0, $storeId);
            $this->addMessage('<strong>Disabled</strong> Admin template path hints', 2);
        }
        // Flush Cache
        $this->flushConfigCache();
    }

    /**
     * Flush config cache
     */
    public function flushConfigCache()
    {
        $cacheManager = $this->getCacheManager();
        $cacheManager->flush(array('full_page'));
    }

    /**
     * Flush Magento Cache
     *
     */
    public function flushCache()
    {
        $cacheManager   = $this->getCacheManager();
        $requestedTypes = isset($_GET['cache_type']) ? $_GET['cache_type'] : false;
        if ($requestedTypes) {
            $requestedTypes = explode(',', $requestedTypes);
        } else {
            $requestedTypes = $cacheManager->getAvailableTypes();
        }
        $cacheManager->flush($requestedTypes);
        $this->addMessage('Flushed cache successfully at ' . date("Y-m-d H:i:s"), 1);
    }

    /**
     * Login as admin
     */
    public function loginAsAdmin()
    {
        setcookie('admin', "", -1, '/');
        $this->getAuthSession()->setName('admin');
        if (!$this->getAuthSession()->isLoggedIn()) {
            $id   = isset($_GET['id']) ? $_GET['id'] : 1;
            $user = $this->getModelUser()->load($id);
            if ($user && $user->getId()) {
                $this->getAuthSession()->setUser($user);
                $this->getAuthSession()->processLogin();
            }
            $redirectUrl = $this->getBackendUrl()->getUrl('admin/dashboard/index');
            header('Location: ' . $redirectUrl);
            exit;
        }
    }

    public function getSessionConfig()
    {
        return $this->getObjectManager()->get('\Magento\Framework\Session\Config\ConfigInterface');
    }

    /**
     * @return mixed
     */
    public function getAuthSession()
    {
        return $this->getObjectManager()->get('\Magento\Backend\Model\Auth\Session');
    }

    /**
     * Login as customer
     *
     * @throws Mage_Core_Exception
     */
    public function loginAsCustomer()
    {
        $id       = isset($_GET['id']) ? $_GET['id'] : 1;
        $customer = $this->getCustomerModel()->load($id);
        if ($customer && $customer->getId()) {
            $this->getCustomerSession()->setCustomerAsLoggedIn($customer);
            $this->getCustomerSession()->regenerateId();
            header('Location: ' . $this->getBaseUrl());
            exit;
        } else {
            $customers = $this->getCustomerFactory()->create();
            if (isset($_GET['limit'])) {
                $customers->setPageSize($_GET['limit']);
            }
            $message = '<p><strong>Customer does not exist! Select a customer to login</strong></p>';
            $message .= '<ol>';
            foreach ($customers as $_customer) {
                $message .= '<li><a target="_blank" href="' . $this->getUrl(array('action' => 'loginAsCustomer', 'id' => $_customer->getId())) . '"> ' . $_customer->getEmail() . ' </a></li>';
            }
            $message .= '</ol>';
            $this->addMessage($message);
        }
    }

    /**
     *
     */
    public function getModelUser()
    {
        return $this->getObjectManager()->get('\Magento\User\Model\User');
    }

    /**
     * @return mixed
     */
    public function getModelAuth()
    {
        return $this->getObjectManager()->get('\Magento\Backend\Model\Auth');
    }

    /**
     * @return mixed
     */
    public function getBackendUrl()
    {
        return $this->getObjectManager()->get('\Magento\Backend\Model\UrlInterface');
    }

    /**
     * @return mixed
     */
    public function getCustomerSession()
    {
        return $this->getObjectManager()->get('\Magento\Customer\Model\Session');
    }

    /**
     * @return mixed
     */
    public function getCustomerModel()
    {
        return $this->getObjectManager()->get('\Magento\Customer\Model\Customer');
    }

    /**
     * @return mixed
     */
    public function getCustomerFactory()
    {
        return $this->getObjectManager()->get('\Magento\Customer\Model\ResourceModel\Customer\CollectionFactory');
    }

    /**
     * Run php command
     */
    public function run()
    {
        if (isset($_GET['cmd'])) {
            $this->addMessage(eval($_GET['cmd']));
        }
    }

    /**
     * @param string $command
     */
    public function magentoCommand()
    {
        $command = isset($_GET['command']) ? $_GET['command'] : '';
        if ($command) {
            $this->addMessage(shell_exec("php bin/magento " . $command), 1);
        }
    }

    /**
     * Backup app, skin, js folder
     */
    public function backup()
    {
        $file    = 'backup-' . date("Y-m-d");
        $cmd     = "tar -czvf $file.tgz skin/ js/ app/";
        $message = "<pre>";
        $message .= "IN  : " . realpath(__FILE__) . "\n";
        $message .= "EXEC: $cmd \n";
        $message .= "php_user@server:\n" . shell_exec('pwd');
        system($cmd . ' &');
        $message .= "Done!</pre>";
        $this->addMessage($message, 1);
    }

    /**
     * Enable/Disable cache
     */
    public function cache()
    {
        $mode           = isset($_GET['mode']) ? $_GET['mode'] : 1;
        $cacheManager   = $this->getCacheManager();
        $requestedTypes = isset($_GET['cache_type']) ? $_GET['cache_type'] : false;
        if ($requestedTypes) {
            $requestedTypes = explode(',', $requestedTypes);
        } else {
            $requestedTypes = $cacheManager->getAvailableTypes();
        }
        $messageType = 1;
        if ($mode == 0) {
            $cacheManager->setEnabled($requestedTypes, 0);
            $messageType = 2;
            $message     = '<strong>Disabled cache types:</strong>';

        } else if ($mode == 1) {
            $cacheManager->setEnabled($requestedTypes, 1);
            $message = '<strong>Enabled cache types:</strong>';

        } else {
            $cacheManager->clean($requestedTypes);
            $message = '<strong>Cleaned cache types:</strong>';
        }
        foreach ($requestedTypes as $type) {
            $message .= $type . ', ';
        }
        $this->addMessage($message, $messageType);
    }

    /**
     * Zip current dir
     */
    public function zipCurrent()
    {
        $this->zip(getcwd(), $_SERVER['HTTP_HOST']);
    }

    /**
     * Zip code
     */
    public function zipCode()
    {
        $ouput_file    = 'patch_' . $_SERVER['HTTP_HOST'] . '.zip';
        $media_exclude = array('.', '..', 'media', 'catalog', 'wysiwyg', 'xmlconnect', 'downloadable', 'dhl', 'customer', 'var', '_tmp', 'tmp', '.htaccess');
        if (isset($_GET['lite'])) {
            $lib_exclude = array('.', '..', '3Dsecure', 'captcha', 'flex', 'googlecheckout', 'LinLibertineFont', '', 'Mage', 'PEAR', 'phpseclib', 'Varien', 'Zend');
        } else {
            $lib_exclude = array('.', '..');
        }

        $folders = array(
            'app/code/'
        );

        $media = scandir('media');
        foreach ($media as $k => $v) {
            if (in_array($v, $media_exclude) OR is_file($v)) {
                unset($media[$k]);
            } else {
                $media[$k] = 'media' . '/' . $v . '/';
            }
        }

        $lib = scandir('lib');
        foreach ($lib as $k => $v) {
            if (in_array($v, $lib_exclude) OR is_file($v)) {
                unset($lib[$k]);
            } else {
                $lib[$k] = 'lib' . '/' . $v . '/';
            }
        }


        $folders = array_merge($folders, $media, $lib);
        try {
            $zip = new ZipArchive();
            if ($zip->open($ouput_file, ZIPARCHIVE::CREATE) == true) {
                //Files
                foreach ($folders as $folder) {
                    if (is_dir($folder)) {
                        $iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($folder));
                        foreach ($iterator as $key => $value) {
                            if (is_file($key)) {
                                $zip->addFile(realpath($key), $key);
                            } else {
                                $zip->addEmptyDir(realpath($key));

                            }

                        }
                    }

                }
            }
            $zip->close();
        } catch (Exception $e) {
            $this->addMessage($e->getMessage(), 2);
        }
        $this->addMessage(' ' . date('Y-m-d H:i:s') . " Archive created successfully. File is: " . '<a href="' . $ouput_file . '" target="_blank">' . $ouput_file . '</a>', 1);
    }

    /**
     * Zip all media files
     */
    public function zipMedia()
    {
        $this->zip('media/', 'media');
    }

    /**
     * @param $fromDir
     * @param $toFile
     */
    public function zip($fromDir, $toFile)
    {
        $file    = $toFile;
        $cmd     = "tar -czvf $file.tgz " . $fromDir;
        $message = "<pre>";
        $message .= "IN  : " . realpath(__FILE__) . "\n";
        $message .= "EXEC: $cmd \n";
        $message .= "php_user@server:\n" . shell_exec('pwd');
        system($cmd . ' &');
        $message .= "Done!</pre>";
        $this->addMessage($message, 1);
    }

    /**
     * Unzip a file
     */
    public function unzip()
    {
        $file = isset($_REQUEST['file']) ? $_REQUEST['file'] : null;
        if (isset($file)) {
            $zip = new ZipArchive;
            $res = $zip->open($file);
            if ($res === true) {
                $zip->extractTo(getcwd());
                $zip->close();
                $this->addMessage('File ' . $file . ' is extracted to ' . getcwd());
            } else {
                $this->addMessage('Can\'t unzip ' . $file . ' file', 2);
            }
        }
    }

    /**
     * View php information
     */
    public function phpInfo()
    {
        phpinfo();
    }

    /**
     * @param        $name
     * @param null   $storeId
     * @param string $stores
     */
    public function toggleConfig($name, $storeId = null)
    {

        $config = $this->getStoreConfig($name, $storeId);
        if ($config == 0) {
            $this->addMessage('<strong>Enabled</strong> ' . $name, 1);
            $this->saveConfig($name, 1, $storeId);

        } else {
            $this->saveConfig($name, 0, $storeId);
            $this->addMessage('<strong>Disabled</strong> ' . $name, 2);
        }
    }

    /**
     * Enable/Disable frontend translate inline
     */
    public function toggleTranslateInline()
    {
        $this->toggleConfig('dev/translate_inline/active', $this->getStoreId());
    }

    /**
     * Enable/Disable backend translate inline
     */

    public function toggleTranslateInlineAdmin()
    {
        $this->toggleConfig('dev/translate_inline/active_admin', $this->getStoreId());
    }

    /**
     * Delete a folder
     */
    public function deleteFolder($dirPath = null)
    {
        if (!$dirPath) {
            $dirPath = isset($_REQUEST['folder']) ? $_REQUEST['folder'] : '';
            if (!$dirPath)
                return;
        }
        if (!is_dir($dirPath)) {
            throw new InvalidArgumentException("$dirPath must be a directory");
        }
        if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
            $dirPath .= '/';
        }
        $files = glob($dirPath . '*', GLOB_MARK);
        foreach ($files as $file) {
            if (is_dir($file)) {
                $this->deleteFolder($file);
            } else {
                unlink($file);
            }
        }
        rmdir($dirPath);
    }

    /**
     * @return bool
     */
    public function deleteFile()
    {
        if (isset($_GET['file']) && is_file($_GET['file'])) {
            try {
                unlink($_GET['file']);
                $this->addMessage($_GET['file'] . ' is deleted', 1);
            } catch (Exception $e) {
                $this->addMessage('Can\'t delete ' . $_GET['file'], 2);
            }
        }
    }

    public function getResourceConnection()
    {
        return $this->getObjectManager()->get('\Magento\Framework\App\ResourceConnection');
    }

    /**
     * Run a query
     */
    public function sqlQuery()
    {
        if (isset($_REQUEST['cmd'])) {
            $connection = $this->getResourceConnection()->getConnection();
            try {
                \Zend_Debug::dump($connection->query($_REQUEST['cmd'])->fetchAll());
            } catch (Exception $e) {
                $this->addMessage($e->getMessage(), 2);
            }
        }
    }

    /**
     * Set Cookie 365 days
     */
    public function adminCookieLifeTime()
    {
        $storeId = 1;
        $this->saveConfig('web/cookie/cookie_lifetime', isset($_GET['value']) ? $_GET['value'] : 86400 * 365, $storeId);
    }

    /**
     * Use admin form key
     */
    public function toggleAdminUseFormKey()
    {
        $this->toggleConfig('admin/security/use_form_key', $this->getStoreId());
    }

    /**
     * Merge css config
     */
    public function toggleMergeCss()
    {
        $this->toggleConfig('dev/css/merge_css_files', $this->getStoreId());
    }

    /**
     * Merge js config
     */
    public function toggleMergeJs()
    {
        $this->toggleConfig('dev/js/merge_files', $this->getStoreId());
    }

}

$dev = new Dev();
?>

<!DOCTYPE html>
<html lang="en-US">
<head>
    <title>Dev tools</title>
    <style type="text/css">
        body {
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            font-size: 14px;
            line-height: 1.42857143;
            color: #333;
            background-color: #fff;
        }

        a {
            text-decoration: none;
            font-weight: 500;
            color: #563d7c;
        }

        .list {
            list-style-type: decimal;
        }

        .list li {
            margin-bottom: 0.2em;
        }

        .success-message {
            transition-duration: 0.3s;
            background: #7db500;
            color: white;
            text-shadow: none;
            padding: 0.5em;
        }

        .notice-message {
            transition-duration: 0.3s;
            background: #b5140a;
            color: white;
            text-shadow: none;
            padding: 0.5em;
        }

        .button {
            background: #00a1cb;
            color: white;
            text-shadow: none;
            /* border: none; */
            -webkit-box-shadow: none;
            -moz-box-shadow: none;
            box-shadow: none;
            -webkit-transition-property: background;
            -moz-transition-property: background;
            -o-transition-property: background;
            transition-property: background;
            -webkit-transition-duration: 0.3s;
            -moz-transition-duration: 0.3s;
            -o-transition-duration: 0.3s;
            transition-duration: 0.3s;
            padding: 3px;
        }

        .nav {
            width: 25%;
            float: left;
            margin-left: 5%;
        }

        .content {
            width: 60%;
            float: left;
            margin-top: 2%;
            margin-right: 5%;
        }

        .content form {
            margin-bottom: 10px;
        }

        .config-name {
            font-weight: bold;
        }

        .input-box label {
            display: block;
            font-weight: bold;
        }

        .input-box input, .input-box textarea {
            width: 350px;
        }

        .button-box {
            margin-top: 5px;
        }

        .message {
            margin-bottom: 10px;
        }

    </style>

</head>


<body>


<div class="nav" style="width: 25%;
	float: left;
	margin-left: 5%;
	">
    <h3>Navigation</h3>
    <ul class="list">
        <li><a href="<?php echo $dev->getUrl(); ?>">Dashboard</a></li>
        <li><a target="_blank" href="<?php echo $dev->getBaseUrl() ?>">Go
                to <?php echo $dev->getBaseUrl() ?></a></li>
        <li>
            <a class="button" href="<?php echo $dev->getUrl(array('action' => 'frontendPH')) ?>">Frontend </a> /
            <a class="button" href="<?php echo $dev->getUrl(array('action' => 'backendPH')) ?>">Backend TPH</a>
        </li>
        <li><a class="button" href="<?php echo $dev->getUrl(array('action' => 'flushCache')) ?>">Flush Cache</a></li>

        <li><a target="_blank" href="<?php echo $dev->getUrl(array('action' => 'loginAsAdmin')) ?>">Login As Admin</a> /
            <a
                target="_blank" href="<?php echo $dev->getUrl(array('action' => 'loginAsCustomer')) ?>">Customer</a>
        </li>

        <li>
            <a href="<?php echo $dev->getUrl(array('action' => 'magentoCommand', 'command' => 'setup:upgrade')) ?>">setup:upgrade</a>
            /
            <a href="<?php echo $dev->getUrl(array('action' => 'magentoCommand', 'command' => 'setup:static-content:deploy')) ?>">setup:static-content:deploy</a>
        </li>

        <li><a href="<?php echo $dev->getUrl(array('action' => 'backup')) ?>">Run Backup: app,js,skin</a></li>
        <li>
            <a href="<?php echo $dev->getUrl(array('action' => 'deleteFolder', 'folder' => urlencode('var/generation/'))) ?>">Delete
                var/generation Folder</a></li>

        <li>
            <a href="<?php echo $dev->getUrl(array('action' => 'cache', 'mode' => 0)) ?>">Disable </a> /
            <a href="<?php echo $dev->getUrl(array('action' => 'cache', 'mode' => 1)) ?>">Enable </a> /
            <a href="<?php echo $dev->getUrl(array('action' => 'cache', 'mode' => 2)) ?>">Clean Cache</a>
        </li>
        <li>
            Zip:
            <a href="<?php echo $dev->getUrl(array('action' => 'zipCurrent')) ?>">Current</a> /
            <a href="<?php echo $dev->getUrl(array('action' => 'zipCode')) ?>">Code</a> /
            <a href="<?php echo $dev->getUrl(array('action' => 'zipMedia')) ?>">Media</a>
        </li>

        <!--        <li><a href="-->
        <?php //echo $dev->getUrl(array('action' => 'showConfig')) ?><!--" target="_blank">Show Config</a></li>-->
        <!--        <li><a href="-->
        <?php //echo $dev->getUrl(array('action' => 'showLayout')) ?><!--" target="_blank">Show Layout</a></li>-->
        <li><a href="<?php echo $dev->getUrl(array('action' => 'unzip')) ?>">Unzip file</a></li>
        <li><a href="<?php echo $dev->getUrl(array('action' => 'phpInfo')) ?>" target="_blank">PHPInfo()</a></li>
        <li><a href="<?php echo $dev->getUrl(array('action' => 'magentoCommand', 'command' => 'indexer:reindex')) ?>">Re-Index</a>
        </li>
        <li><a href="<?php echo $dev->getUrl(array('action' => 'toggleTranslateInline')) ?>">Frontend / <a
                    href="<?php echo $dev->getUrl(array('action' => 'toggleTranslateInlineAdmin')) ?>">Backend</a>
                Translate Inline</a>
        </li>
        <li><a href="<?php echo $dev->getUrl(array('action' => 'deleteFolder')) ?>">Delete Folder</a></li>
        <li><a href="<?php echo $dev->getUrl(array('action' => 'deleteFile')) ?>">Delete File</a></li>
        <li><a href="<?php echo $dev->getUrl(array('action' => 'run')) ?>">Run</a></li>
        <li><a href="<?php echo $dev->getUrl(array('action' => 'magentoCommand')) ?>">Run Magento Command</a></li>
        <li><a href="<?php echo $dev->getUrl(array('action' => 'sqlQuery')) ?>">Run sql Query</a></li>
        <!--        <li><a href="-->
        <?php //echo $dev->getUrl(array('action' => 'getAllStoreConfigs')) ?><!--">Show All Store Configs</a>-->
        <li><a href="<?php echo $dev->getUrl(array('action' => 'setStoreConfig')) ?>">Set Store Config</a></li>
        <li><a href="<?php echo $dev->getUrl(array('action' => 'adminCookieLifeTime')) ?>">Admin Cookie LifeTime - 365
                days</a></li>
        <li><a href="<?php echo $dev->getUrl(array('action' => 'toggleAdminUseFormKey')) ?>">Admin Use Form Key</a></li>
        <li>
            <a href="<?php echo $dev->getUrl(array('action' => 'toggleMergeCss')) ?>">Merge CSS</a> /
            <a href="<?php echo $dev->getUrl(array('action' => 'toggleMergeJs')) ?>">Js</a>
        </li>
        <li><a href="<?php echo $dev->getUrl(array('action' => 'selfRemove')) ?>">Self-Remove</a></li>


    </ul>


</div>

<div class="content" style="width: 60%;
	float: left;
	margin-top: 2%;
	margin-right: 5%;">

    <?php
    if (isset($_SESSION['message']) && !empty($_SESSION['message'])) {
        echo '<div class="message ' . (isset($_SESSION['message_type']) ? $_SESSION['message_type'] : 'message') . '">' . $_SESSION['message'] . '</div>';
        unset($_SESSION['message']);
        unset($_SESSION['message_type']);
    }
    ?>
    <?php if ($dev->getAction() == 'unzip') { ?>
        <form action="" method="POST">
            <input type="hidden" name="action" value="unzip"/>

            <div class="input-box">
                <input type="text" placeholder="Enter File.zip in current Dir" name="file" size="100">
            </div>
            <div class="button-box">
                <input type="submit" name="unzipnow" value="UNZIP now">
            </div>
        </form>
    <?php } ?>

    <?php if ($dev->getAction() == 'setStoreConfig') { ?>
        <form action="" method="GET">
            <input type="hidden" name="action" value="setStoreConfig"/>

            <div class="input-box">
                <label for="config_path">
                    Config Path
                </label>
                <input type="text" placeholder="Path" name="path" id="config_path"
                       value="<?php echo isset($_REQUEST['path']) ? $_REQUEST['path'] : '' ?>" size="100">
            </div>
            <div class="input-box">
                <label for="config_value">
                    Value
                </label>
                 <textarea placeholder="" cols="10" rows="3" id="config_value"
                           name="value"><?php echo isset($_REQUEST['value']) ? $_REQUEST['value'] : '' ?></textarea>
            </div>
            <div class="button-box">
                <input type="submit" name="saveStoreConfig" value="Save">
            </div>
        </form>
    <?php } ?>

    <?php if ($dev->getAction() == 'deleteFolder') { ?>
        <form action="" method="POST">
            <input type="hidden" name="action" value="deleteFolder"/>

            <div class="input-box">
                <input type="text" placeholder="Enter Folder in current Dir" name="folder" size="100" value="">
            </div>
            <div class="button-box">
                <input type="submit" name="deleteFolderNow" value="DELETE now">
            </div>
        </form>
    <?php } ?>

    <?php if ($dev->getAction() == 'deleteFile') { ?>
        <form action="" method="GET">
            <input type="hidden" name="action" value="deleteFile"/>

            <div class="input-box">
                <input type="text" placeholder="Enter file name" name="file" size="100" value="">
            </div>
            <div class="button-box">
                <input type="submit" name="deleteFileNow" value="DELETE now">
            </div>
        </form>
    <?php } ?>

    <?php if ($dev->getAction() == 'run') { ?>
        <form action="" method="GET">
            <input type="hidden" name="action" value="run"/>

            <div class="input-box">
                 <textarea placeholder="" cols="80" rows="10"
                           name="cmd"><?php echo isset($_GET['cmd']) ? $_GET['cmd'] : null ?></textarea>
            </div>
            <div class="button-box">
                <input type="submit" name="run" value="Run now">
            </div>
        </form>
    <?php } ?>
    <?php if ($dev->getAction() == 'magentoCommand') { ?>
        <form action="" method="GET">
            <input type="hidden" name="action" value="magentoCommand"/>

            <div class="input-box">
                 <textarea placeholder="" cols="80" rows="10"
                           name="command"><?php echo isset($_GET['command']) ? $_GET['command'] : null ?></textarea>
            </div>
            <div class="button-box">
                <input type="submit" name="run" value="Run now">
            </div>
        </form>
    <?php } ?>
    <?php if ($dev->getAction() == 'sqlQuery') { ?>
        <form action="" method="GET">
            <input type="hidden" name="action" value="sqlQuery"/>

            <div class="input-box">
                 <textarea placeholder="" cols="80" rows="10"
                           name="cmd"><?php echo isset($_GET['cmd']) ? $_GET['cmd'] : null ?></textarea>
            </div>
            <div class="button-box">
                <input type="submit" name="sqlQuery" value="Run SQL">
            </div>
        </form>
    <?php } ?>
    <?php $dev->getBasicConfigs(); ?>
    Magento version <strong><?php echo \Magento\Framework\AppInterface::VERSION ?></strong> <br>
    Path: <strong><?php echo $_SERVER['SCRIPT_FILENAME'] ?></strong> <br>
    PHP: <strong><?php echo phpversion() ?></strong> <br>
    Server time: <strong><?php echo date("Y-m-d H:i:s", time()) ?></strong> <br>


</div>
</body>
</html>


